/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.fxml.Initializable;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author preeti
 */
public class ManageSupplierController implements Initializable {

    /**
     * Initializes the controller class.
     */

//    @FXML
//    private AnchorPane search;
//
//    @FXML
//    private TextField search_text;
    @FXML
    private TableView<Supplier> table;

    @FXML
    private TableColumn<Supplier, Integer> id_col;

    @FXML
    private TableColumn<Supplier, String> name_col;

    @FXML
    private TableColumn<Supplier, String> gstNo_col;

    @FXML
    private TableColumn<Supplier, String> phoneNo_col;

    @FXML
    private TableColumn<Supplier, String> email_col;

    @FXML
    private TableColumn<Supplier, String> company_name_col;

    @FXML
    private TableColumn<Supplier, String> address_col;
    
     @FXML
    private TableColumn<Supplier, String> action_col;

    @FXML
    private TextField search_text;
   
    
     @FXML
    private AnchorPane form;

    @FXML
    private Pane pane;

    @FXML
    private TextField lnametxt;

    @FXML
    private TextField fnametxt;

    @FXML
    private Button submit;

    @FXML
    private TextField gstnotxt;

    @FXML
    private TextField phonenumbertxt;

    @FXML
    private TextField emailidtxt;

    @FXML
    private TextField blocknotxt;

    @FXML
    private TextField streetnotxt;

    @FXML
    private TextField citytxt;

    @FXML
    private TextField pincodetxt;

    @FXML
    private TextField statetxt;

    @FXML
    private TextField countrytxt;

    @FXML
    private TextField towntxt;

    @FXML
    private TextField companyNametxt;

    @FXML
    private TextField hiddenTextField;
        
    public ObservableList<Supplier> getSupplierList(){
        ObservableList<Supplier> supplier = FXCollections.observableArrayList();
         String sql = "SELECT s.id, concat(s.first_name,\" \",s.last_name) as full_name, s.gst_no, s.email_id, s.phone_no, s.company_name, concat(a.block_no,\";\",a.street,\";\",a.city,\";\",a.pincode,\";\",a.state,\";\",a.country,\";\",a.town) as full_address\n" +
                        "from suppliers s\n" +
                        "INNER JOIN address_supplier \n" +
                         "ON s.id = address_supplier.supplier_id\n" +
                          "INNER JOIN address a\n" +
                            "on a.id = address_supplier.address_id\n" +
                            "WHERE s.deleted = 0 AND a.deleted = 0;";
         
         ResultSet rs = DbConnect.execute(sql);
            try {
                while(rs.next()){
                    String name = rs.getString("full_name");
                supplier.add(new Supplier(name,rs.getString("email_id"),rs.getString("company_name"), rs.getString("full_address"), rs.getString("phone_no"), rs.getString("gst_no"), rs.getInt("id")));
                } 
            } catch (SQLException ex) {   
                ex.printStackTrace();
    }   
    return supplier;    
 }
    
    
       public void dispose() {
        //scrollPane.setVisible(false);
             
             PreparedStatement ps =  null;
             PreparedStatement ps2 =  null;
         try {
             if((!"".equals(fnametxt.getText())) 
                     && (!"".equals(lnametxt.getText())) 
                     && (!"".equals(gstnotxt.getText()))
                     && (!"".equals(phonenumbertxt.getText()))
                     && (!"".equals(emailidtxt.getText())) 
                     && (!"".equals(companyNametxt.getText()))
                     && (!"".equals(blocknotxt.getText()))
                     && (!"".equals(streetnotxt.getText()))
                     && (!"".equals(citytxt.getText()))
                     && (!"".equals(statetxt.getText()))
                     && (!"".equals(countrytxt.getText()))
                     && (!"".equals(towntxt.getText()))
                     )  {
                 
            Connection conn = DbConnect.getConnection();
            java.util.Date dt = new java.util.Date();
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTime = sdf.format(dt);
            
             
            String sql = "UPDATE suppliers SET first_name = ?, last_name = ?, gst_no =?, phone_no = ?, email_id = ?, company_name = ?, updated_at = ? where id = ? ";
            String sql1 = "SELECT address_id from address_supplier where supplier_id ="+ hiddenTextField.getText();
            ResultSet rs = DbConnect.execute(sql1);
            rs.next();
            
            int addressId = rs.getInt("address_id");
            System.out.println(addressId);
            String sql2 = "UPDATE address SET block_no = ?, street = ?,city = ?, pincode=?, state=?, country=?, town=?, updated_at = ? where id= ?";
            
            
            ps = conn.prepareStatement(sql);
            ps2 = conn.prepareStatement(sql2);
            
            ps.setString(1,fnametxt.getText());
            ps.setString(2,lnametxt.getText());
            ps.setString(3,gstnotxt.getText());
            ps.setString(4,phonenumbertxt.getText());
            ps.setString(5,emailidtxt.getText());
            ps.setString(6,companyNametxt.getText());
            ps.setString(7, currentTime);
            ps.setString(8, hiddenTextField.getText());
            
            ps2.setString(1,blocknotxt.getText());
            ps2.setString(2,streetnotxt.getText());
            ps2.setString(3,citytxt.getText());
            ps2.setString(4,pincodetxt.getText());
            ps2.setString(5,statetxt.getText());
            ps2.setString(6,countrytxt.getText());
            ps2.setString(7,towntxt.getText());
            ps2.setString(8,currentTime);
            ps2.setInt(9,addressId);
              
            ps.executeUpdate();
            ps2.executeUpdate();
            
            showSuppliers();
          }
        } catch (SQLException ex) {
            Logger.getLogger(DbConnect.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
 
    
    public void showSuppliers(){
    
        System.out.println("Here");
         
    ObservableList<Supplier> list = getSupplierList();
    
    id_col.setCellValueFactory(new PropertyValueFactory<>("id"));
    name_col.setCellValueFactory(new PropertyValueFactory<>("name"));
    phoneNo_col.setCellValueFactory(new PropertyValueFactory<>("phoneNo"));
    gstNo_col.setCellValueFactory(new PropertyValueFactory<>("gstNo"));
    email_col.setCellValueFactory(new PropertyValueFactory<>("email"));
    company_name_col.setCellValueFactory(new PropertyValueFactory<>("company_name"));
    address_col.setCellValueFactory(new PropertyValueFactory<>("address"));

//Addong buttons in each cell
Callback<TableColumn<Supplier,String>,TableCell<Supplier,String>> cellFactory = (param)->{
    
    final TableCell<Supplier,String> cell = new TableCell<Supplier, String>(){
        
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if(empty) {
                setGraphic(null);
                setText(null);
            }
    else {
                Image img = new Image("trash.png");
                ImageView view = new ImageView(img);
                Image img1 = new Image("pen.png");
                ImageView view1 = new ImageView(img1);
                
                final Button deleteButton = new Button();
                final Button editButton = new Button();
                final HBox pane = new HBox(deleteButton, editButton);
                deleteButton.setStyle("-fx-background-color: transparent;");
                editButton.setStyle("-fx-background-color: transparent;");
                editButton.setGraphic(view1);
                         
                
                deleteButton.setGraphic(view);
                deleteButton.setOnAction(event -> { 
                Supplier s = getTableView().getItems().get(getIndex());
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Are you sure you want to delete " + " " + s.getName() + " " + "record ? ");
                 String sql= "UPDATE suppliers SET deleted = 1 WHERE id = "+ s.getId();
                 Optional<ButtonType> result = alert.showAndWait();
                  if(!result.isPresent()) {
                     // alert is exited, no button has been pressed.
                  }
    
                    else if(result.get() == ButtonType.OK)
                    {
                        //oke button is pressed
                        DbConnect.delete(sql);
                        showSuppliers();
                    }
     
                    else if(result.get() == ButtonType.CANCEL) {
                        // cancel button is pressed
                    }
                });
                editButton.setOnAction(event -> { 
                 
                        Supplier s = getTableView().getItems().get(getIndex());
                        //scrollPane.setVisible(true);
                        form.setVisible(true);
                        String employeeName = s.getName();
                        String[] words = employeeName.split("\\s");
                            fnametxt.setText(words[0]);
                            lnametxt.setText(words[1]);
                         gstnotxt.setText(s.getGstNo());
                        emailidtxt.setText(s.getEmail());
                        companyNametxt.setText(s.getCompany_name());
                        hiddenTextField.setText(s.getId()+"");
                        phonenumbertxt.setText(s.getPhoneNo()); 
                        
                        String address = s.getAddress();
                        String addresswords[] = address.split(";");
                        
                        System.out.println(addresswords);
                        blocknotxt.setText(addresswords[0]);
                        streetnotxt.setText(addresswords[1]);
                        citytxt.setText(addresswords[2]);
                        pincodetxt.setText(addresswords[3]);
                        statetxt.setText(addresswords[4]);
                        countrytxt.setText(addresswords[5]);
                        towntxt.setText(addresswords[6]);  
                        
    });


                setGraphic(pane);
                setText(null);
            }
        }
    };
 return cell;
};
action_col.setCellFactory(cellFactory);
table.setItems(list);
    }    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        showSuppliers();
        form.setVisible(false);
        hiddenTextField.setVisible(false);
    }    
}