

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
//import controlsfx.validation.ValidationSupport;
//import controlsfx.validation.Validator;

/**
 * FXML Controller class
 *
 * @author Jayesh Karia
 */
public class AddCustomerController implements Initializable {
    
    @FXML
    private ComboBox<String> gender;
      @FXML
    private AnchorPane scrollablePane;

    @FXML
    private Pane pane;

    @FXML
    private TextField lname;

    @FXML
    private TextField fname;

    @FXML
    private Button submit;

    @FXML
    private TextField gstno;

    @FXML
    private TextField phonenumber;

    @FXML
    private TextField emailid;

    @FXML
    private TextField blockno;

    @FXML
    private TextField streetno;

    @FXML
    private TextField city;

    @FXML
    private TextField pincode;

    @FXML
    private TextField state;

    @FXML
    private TextField country;

    @FXML
    private TextField town;

   // ValidationSupport validationsupport = new ValidationSupport();
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    gender.getItems().removeAll(gender.getItems());
    gender.getItems().addAll("Male", "Female");
    
    
//    validationsupport.registerValidator(fname, Validator.createEmptyValidator("Text is required"));
//    validationsupport.registerValidator(lname, Validator.createEmptyValidator("Text is required"));
//    validationsupport.registerValidator(gstno, Validator.createEmptyValidator("Text is required"));
//    validationsupport.registerValidator(phonenumber, Validator.createEmptyValidator("Text is required"));
//    validationsupport.registerValidator(emailid, Validator.createEmptyValidator("Text is required"));
//    validationsupport.registerValidator(blockno, Validator.createEmptyValidator("Text is required"));
//    validationsupport.registerValidator(streetno, Validator.createEmptyValidator("Text is required"));
//    validationsupport.registerValidator(state, Validator.createEmptyValidator("Text is required"));
//    validationsupport.registerValidator(country, Validator.createEmptyValidator("Text is required"));
//    validationsupport.registerValidator(town, Validator.createEmptyValidator("Text is required"));
//    validationsupport.registerValidator(pincode, Validator.createEmptyValidator("Text is required"));
//    validationsupport.registerValidator(city, Validator.createEmptyValidator("Text is required"));
//       
    }   
    
      public void display(){
      submit.setOnAction(e->{
          try {
              
              
              Connection conn = DbConnect.getConnection();
              String sql = "INSERT into customers (first_name,last_name,gst_no,email_id,phone_no,gender)" + "values(?,?,?,?,?,?)";
              String sql1 = "INSERT INTO address (block_no,street,city,pincode,state,country,town)"+"values(?,?,?,?,?,?,?)";
              String sql2 = "select id from address";
              String sql3 = "select id from customers";
              String sql4 = "INSERT into address_customer (address_id,customer_id) values (?,?)";
              
              PreparedStatement ps = conn.prepareStatement(sql);
              PreparedStatement ps1 = conn.prepareStatement(sql1);
              PreparedStatement ps2 = conn.prepareStatement(sql2);
              PreparedStatement ps3 = conn.prepareStatement(sql3);
              PreparedStatement ps4 = conn.prepareStatement(sql4);
              
              ps.setString(1,fname.getText());
              ps.setString(2,lname.getText());
              ps.setString(3,gstno.getText());
              ps.setString(4,emailid.getText());
              ps.setString(5,phonenumber.getText());
              ps.setString(6,gender.getValue());
              ps.execute();
              ps1.setString(1,blockno.getText());
              ps1.setString(2,streetno.getText());
              ps1.setString(3,city.getText());
              ps1.setString(4,pincode.getText());
              ps1.setString(5,state.getText());
              ps1.setString(6,country.getText());
              ps1.setString(7,town.getText());
                
              ps1.execute();
              ResultSet rs = ps2.executeQuery();
              rs.last();
              ResultSet rs1 = ps3.executeQuery();
              rs1.last();
              
              int addressId = rs.getInt("id");
              int customerId = rs1.getInt("id");
              ps4.setInt(1,addressId);
              ps4.setInt(2,customerId);
              ps4.execute();
           
              
              
          } catch (SQLException ex) { 
              Logger.getLogger(AddEmployeeController.class.getName()).log(Level.SEVERE, null, ex);
          }
          try {
           FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("dashboard.fxml"));
           Parent  root1 = (Parent) fxmlLoader.load();
           Stage stage = new Stage();
           stage.setScene(new Scene(root1));  
           stage.show();
          } catch (IOException ex) {
              Logger.getLogger(AddEmployeeController.class.getName()).log(Level.SEVERE, null, ex);
          }
        });
}
    
    
    
}

