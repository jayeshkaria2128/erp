import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


import javafx.stage.Stage;


public class AddSalesController implements Initializable {
    
    @FXML
    private ComboBox<String> category;

    @FXML
    private ComboBox<String> products;
    
     @FXML
    private TextField finaltotal;
    
    @FXML
    private TextField finalrate;
    
    @FXML
    private TextField discount = null;
    
    @FXML
    private TextField quantity;
    
    private String selectedCategory;
    
    private String  selectedProduct;
      @FXML
    private TextField sellingrate;
      @FXML
    private TextField emailtf;
      
    private int customerId;
    
    @FXML
    private Button email;

    @FXML
    private Label label;
    
    private int selectedInvoiceId;
    
    @FXML
    private Button addCustomer;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Helper helper = new Helper();
        helper.addingStartingItemInCombobox(category, products);
        HashMap categoryList = new HashMap();
        categoryList = DbConnect.getCategory();
        helper.setDataInComboboxAsHashMap(category, categoryList);
        addCustomer.setVisible(false);
    }
    
    public void stateChanged() {
        Helper helper = new Helper();
        selectedCategory = category.getSelectionModel().getSelectedItem();
        String[] selectedCat= selectedCategory.split("\\s");
        String s = selectedCat[0];
        HashMap prod = DbConnect.getProductsAccorgingToCategory(s);
        helper.setDataInComboboxAsHashMap(products, prod);
        
    }
    
    public void stateChangedOfProduct(){
        selectedCategory =  category.getSelectionModel().getSelectedItem();
        selectedProduct = products.getSelectionModel().getSelectedItem();
        String[] selectedCat= selectedCategory.split("\\s");
        String s = selectedCat[0];
         String[] selectedProd= selectedProduct.split("\\s");
        String s1 = selectedProd[0];
        int sellingRate = DbConnect.getSellingRate(s,s1);
        sellingrate.setText(sellingRate+"");
    }
    
    public void getQuantity() {
         if(!"".equals(quantity.getText())){
        int totalQuantity = Integer.parseInt(quantity.getText());
        int getSellingRate = Integer.parseInt(sellingrate.getText());
        int total = getSellingRate * totalQuantity;
        finalrate.setText(total+"");
        
        if(!"".equals(discount.getText())) {
        int getDiscountRate = Integer.parseInt(discount.getText());
        double discountedRate  = total * getDiscountRate/100;
        total -= discountedRate;
        finalrate.setText(total+"");
        finaltotal.setText(total+"");
        }
    }
    }
    
    public void submit(){
        try {
            String sql1 = "INSERT INTO invoice (customer_id) VALUES (?)";
            String sql2 = "SELECT id from invoice";
            String sql3 = "INSERT INTO sales (product_id,quantity,discount,invoice_id) VALUES (?,?,?,?)";
            
            
            PreparedStatement ps1 = null;
            PreparedStatement ps2 = null;
            
            Connection conn = DbConnect.getConnection();
            ps1 = conn.prepareStatement(sql1);
            ps1.setInt(1,customerId);
            ps1.execute();
            
            ResultSet rs = DbConnect.execute(sql2);
            rs.last();
            selectedInvoiceId = rs.getInt("id");
            
            ps2 = conn.prepareStatement(sql3);
            String[] selectedProd= selectedProduct.split("\\s");
            String s1 = selectedProd[0];
            ps2.setInt(1,Integer.parseInt(s1));
            ps2.setInt(2,Integer.parseInt(quantity.getText()));
            ps2.setInt(3,Integer.parseInt(discount.getText()));
            ps2.setInt(4,selectedInvoiceId);
            
            ps2.execute();
            
            
            
        } catch (SQLException ex) {
            Logger.getLogger(AddSalesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void checkEmail() throws IOException{
        String sql = "select id from customers where email_id = ?";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try
        {
        Connection conn = DbConnect.getConnection();
        ps = conn.prepareStatement(sql);
        ps.setString(1,emailtf.getText());
        rs = ps.executeQuery();
            if(rs.next()) {
                  customerId = rs.getInt("id");
                  label.setText("Email Verified");
                  label.setStyle("-fx-text-fill: white;");
                  label.setPadding(new Insets(10));
                  label.setStyle("-fx-background-color: GREEN;");
            }else {
                label.setText("Email Not Verified");
                label.setStyle("-fx-text-fill: white;");
                label.setPadding(new Insets(10));
                label.setStyle("-fx-background-color: rgb(231, 74, 59);");
           
                addCustomer.setVisible(true);
                addCustomer.setText("ADD Customer");
                addCustomer.setStyle("-fx-text-fill: white; ");
                addCustomer.setPadding(new Insets(10));   
                addCustomer.setStyle("-fx-background-color: YELLOW;");
                addCustomer.setOnAction(e->{
           
                try {
           FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddCustomer.fxml"));
           Parent  root1 = (Parent) fxmlLoader.load();
           Stage stage = new Stage();
           stage.setScene(new Scene(root1));  
           stage.show();
          } catch (IOException ex) {
              Logger.getLogger(AddEmployeeController.class.getName()).log(Level.SEVERE, null, ex);
          }
           });
                   }
       
        } catch (SQLException ex) {
            Logger.getLogger(AddSalesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
       
        
    }
    

}