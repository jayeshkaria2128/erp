public class Invoice {
    
    String product, specifications;
    int price, quantity, discount, id;
    double total;

    public Invoice(String product, String specifications, int price, int quantity, int discount, double total) {
        this.id = id;
        this.product = product;
        this.specifications = specifications;
        this.price = price;
        this.quantity = quantity;
        this.discount = discount;
        this.total = total;
            }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    

    public int getDiscount() {
        return discount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    
    
    
    
}
