import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;


/**
 * FXML Controller class
 *
 * @author Jayesh Karia
 */
public class ManageProductController implements Initializable {
    
    
@FXML
private TableView<Products> table;


 @FXML
  private TableColumn<Products,Integer> id;

    
    @FXML
    private TableColumn<Products,String> productname;

    @FXML
    private TableColumn<Products,String> specifications;

    @FXML
    private TableColumn<Products,Integer> sellingrate;
    
    
    @FXML
    private TableColumn<Products,Integer> quantity;

    @FXML
    private TableColumn<Products,String> wef;

    @FXML
    private TableColumn<Products,Integer> eoq;

    @FXML
    private TableColumn<Products,Integer> dangerlevel;

    @FXML
    private TableColumn<Products,String> categoryname;

    @FXML
    private TableColumn<Products,String> suppliername;

    @FXML
    private TableColumn<Products,String> action;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        showProducts();
    }
    
      public ObservableList<Products> getProductsList(){
          int count = 0;
        ObservableList<Products> products = FXCollections.observableArrayList();
         String sql = "SELECT products.id, products.name as product_name, products.specification, products.quantity, products.eoq_level, products.danger_level, category.name as category_name, GROUP_CONCAT(CONCAT(suppliers.first_name,\" \",suppliers.last_name) SEPARATOR ' | ') as supplier_name, products_selling_rate.selling_rate, products_selling_rate.with_effect_from FROM products_selling_rate  INNER JOIN (SELECT product_id, MAX(with_effect_from) as wef FROM (SELECT product_id, with_effect_from FROM products_selling_rate WHERE with_effect_from<=CURRENT_TIMESTAMP) as temp GROUP BY temp.product_id) as max_date_table ON max_date_table.product_id = products_selling_rate.product_id AND products_selling_rate.with_effect_from = max_date_table.wef INNER JOIN products ON products.id = products_selling_rate.product_id INNER JOIN category ON category.id = products.category_id INNER JOIN product_supplier ON product_supplier.product_id = products.id INNER JOIN suppliers ON suppliers.id = product_supplier.supplier_id WHERE products.deleted = 0 GROUP BY products.id";
   
            try {
                ResultSet rs = DbConnect.execute(sql);
                while(rs.next()){
                    products.add(new Products(rs.getInt("id"),rs.getString("product_name"),rs.getString("specification"),
                            rs.getInt("quantity"),rs.getInt("selling_rate"),rs.getString("with_effect_from"),
                            rs.getInt("eoq_level"),rs.getInt("danger_level"),rs.getString("category_name"),
                            rs.getString("supplier_name")));
                } 
            } catch (SQLException ex) {
                Logger.getLogger(ManageProductController.class.getName()).log(Level.SEVERE, null, ex);
            }
         
                
                
                 
     
    return products;    
    }
      
     public void showProducts(){
    
    ObservableList<Products> list = getProductsList();
    
    id.setCellValueFactory(new PropertyValueFactory<>("id"));
    productname.setCellValueFactory(new PropertyValueFactory<>("productName"));
    specifications.setCellValueFactory(new PropertyValueFactory<>("specifications"));
    sellingrate.setCellValueFactory(new PropertyValueFactory<>("sellingRate"));
    quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
    wef.setCellValueFactory(new PropertyValueFactory<>("wef"));
    eoq.setCellValueFactory(new PropertyValueFactory<>("eoq"));
    dangerlevel.setCellValueFactory(new PropertyValueFactory<>("dangerLevel"));
    categoryname.setCellValueFactory(new PropertyValueFactory<>("categoryName"));
    suppliername.setCellValueFactory(new PropertyValueFactory<>("supplierName"));
    
    Callback<TableColumn<Products,String>,TableCell<Products,String>> cellFactory = (param)->{
    
    final TableCell<Products,String> cell = new TableCell<Products,String>(){
        
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if(empty) {
                setGraphic(null);
                setText(null);
            }
            else {
                Image img = new Image("trash.png");
                ImageView view = new ImageView(img);
                Image img1 = new Image("pen.png");
                ImageView view1 = new ImageView(img1);
                final Button deleteButton = new Button();
                final Button editButton = new Button();
                final HBox pane = new HBox(deleteButton, editButton);
                deleteButton.setStyle("-fx-background-color: transparent;");
                editButton.setStyle("-fx-background-color: transparent;");
                editButton.setGraphic(view1);
                
                deleteButton.setGraphic(view);

                deleteButton.setOnAction(event -> { 
                Products p = getTableView().getItems().get(getIndex());
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Are you sure you want to delete " + " " + p.getProductName() + " " + " ? ");
//                 String sql= "UPDATE employees SET deleted = 1 WHERE id = "+ p.get();
                 Optional<ButtonType> result = alert.showAndWait();
                  if(!result.isPresent()) {
                     // alert is exited, no button has been pressed.
                  }
    
                    else if(result.get() == ButtonType.OK)
                    {
                        //oke button is pressed
//                        DbConnect.delete(sql);
                        showProducts();
                    }
     
                    else if(result.get() == ButtonType.CANCEL) {
                        // cancel button is pressed
                    }
                });
                 editButton.setOnAction(event -> { 
                 
                      
    });
               
       setGraphic(pane);
       setText(null);
            }
        }
    };
 return cell;
};
action.setCellFactory(cellFactory);
    
    
    
    //action.setCellFactory(cellFactory);
    table.setItems(list);
  }

}

       
    

