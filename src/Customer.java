



public class Customer {
     String name, gstNo,email,gender,address,phoneNo;
     int id;
    
     
     
    
    Customer(int id ,String name, String gstNo,String phoneNo,String email,String gender,String address){
        this.id = id;
        this.name = name;
        this.gstNo = gstNo;
        this.phoneNo = phoneNo;
        this.email = email;
        this.gender = gender;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public int getId() {
        return id;
    }
    
    
    
    
}
