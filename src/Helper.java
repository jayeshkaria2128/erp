
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.control.ComboBox;

public class Helper {

    void setDataInCombobox(ComboBox cmb, ArrayList arrList) {
       
       //converting arrayList into ObservableList
       ObservableList<String> ol = FXCollections.observableList(arrList);
       cmb.setItems(ol);
    }
    
    void setDataInComboboxAsHashMap(ComboBox cmb, HashMap hm) {
       
        //converting arrayList into ObservableList
       ObservableMap<String,String> om = FXCollections.observableMap(hm);
        ArrayList arr = new ArrayList();
            for (Iterator it = hm.entrySet().iterator(); it.hasNext();) {
            Map.Entry<Integer, String> entry = (Map.Entry<Integer, String>) it.next();
            Integer key = entry.getKey();
            String value = entry.getValue();
            arr.add(key+" "+value);
        }
            
        ObservableList<String> ol = FXCollections.observableList(arr);
        cmb.setItems(ol);
   
   }
       
    void addingStartingItemInCombobox(ComboBox category, ComboBox products){
        category.setValue("Choose Category");
        products.setValue("Choose Products");
        
    }
    ArrayList insertDataInArrayList(ResultSet rs, String col) {
        ArrayList arrlist = new ArrayList<>();
        try {
            while(rs.next()) {
                arrlist.add(rs.getString(col));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
        }
    return arrlist;
    }
    
    
    HashMap insertDataInHashmap(ResultSet rs, String col,String id) {
        HashMap hashmap = new HashMap();
        try {
            while(rs.next()) {
                hashmap.put(rs.getInt(id),rs.getString(col));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
        }
    return hashmap;
    }
    
}
