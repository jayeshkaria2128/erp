
import javafx.scene.control.Button;


public class Employee {
     String name,email,gender,address,phoneNo, Buttons;
     int id;
     Button deleteButton;
     Button editButton;
     
     
    
    Employee(int id ,String name,String phoneNo,String email,String gender,String address){
        this.id = id;
        this.name = name;
        this.phoneNo = phoneNo;
        this.email = email;
        this.gender = gender;
        this.address = address;
        
        this.deleteButton = new Button("Delete");
        this.editButton = new Button("Edit");
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(Button deleteButton) {
        this.deleteButton = deleteButton;
    }

    public Button getEditButton() {
        return editButton;
    }

    public void setEditButton(Button editButton) {
        this.editButton = editButton;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public int getId() {
        return id;
    }
    
    
    
    
}
