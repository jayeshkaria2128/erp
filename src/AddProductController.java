import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;


public class AddProductController implements Initializable {
       @FXML
    private ComboBox<String> category;

    @FXML
    private ComboBox<String> suppliers;

    @FXML
    private Button submit;

    @FXML
    private ComboBox<String> hsncode;
    
    
    @FXML
    private TextField productname;

    @FXML
    private TextField specifications;
    
    @FXML
    private TextField eoq;

    @FXML
    private TextField dangerlevel;

    @FXML
    private TextField quantity;

    @FXML
    private TextField sellingrate;
    
    private String selectedCategory = " ";
    
    private String selectedSupplier = " ";
    
    ValidationSupport validationsupport = new ValidationSupport();
    
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Helper helper = new Helper();
        category.setValue("Choose Category:");
        suppliers.setValue("Choose Suppliers:");
        hsncode.setValue("Choose HSN Code:");
        
//        HashMap<String, String> categoryList = new HashMap<>();
//        categoryList = DbConnect.getCategory();
//        helper.setDataInCombobox(category, categoryList);

        HashMap<String, String> categoryList = new HashMap<>();
        categoryList = DbConnect.getCategory();
        helper.setDataInComboboxAsHashMap(category, categoryList);
        
        HashMap<String, String> HSNCodeList = new HashMap<>();
        HSNCodeList = DbConnect.getHSNCode();
        helper.setDataInComboboxAsHashMap(hsncode, HSNCodeList);
        
        validationsupport.registerValidator(productname, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(sellingrate, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(quantity, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(dangerlevel, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(eoq, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(specifications, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(suppliers, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(category, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(hsncode, Validator.createEmptyValidator("Text is required"));
    
    
    }   
    
    public void getSupplierList(){
        Helper helper = new Helper();
        HashMap<String, String> supplierList = new HashMap<>();
        selectedCategory = category.getSelectionModel().getSelectedItem();
        String[] words=selectedCategory.split("\\s");
        String Selectedcate = words[0];
        supplierList = DbConnect.getSuppliers(Selectedcate);
        helper.setDataInComboboxAsHashMap(suppliers, supplierList);
    }
    
    public void display(){
      submit.setOnAction(e->{
          
              Connection conn = DbConnect.getConnection();
              selectedCategory = category.getSelectionModel().getSelectedItem();
              String[] words=selectedCategory.split("\\s");
              int categoryId = Integer.parseInt(words[0]);
              String  hsnCode = hsncode.getSelectionModel().getSelectedItem();
              String[] selectedHsn=hsnCode.split("\\s");
              int HSNCode = Integer.parseInt(selectedHsn[1]);
              int prod_id=0;
              
              String sql = "INSERT into products (name, specification, hsn_code, category_id, eoq_level, "
                      + "danger_level, quantity)" + "values(?,?,?,?,?,?,?)";
              String sql1 = "SELECT id FROM products";

         try {
              PreparedStatement ps = conn.prepareStatement(sql);
              ps.setString(1,productname.getText());
              ps.setString(2,specifications.getText());
              ps.setInt(3,HSNCode);
              ps.setInt(4,categoryId);
              ps.setInt(5,Integer.parseInt(eoq.getText()));
              ps.setInt(6,Integer.parseInt(dangerlevel.getText()));
              ps.setInt(7,Integer.parseInt(quantity.getText()));
              ps.execute();
              
              PreparedStatement ps1 = conn.prepareStatement(sql1);
              ResultSet rs = ps1.executeQuery();
              rs.last();
              
              prod_id = rs.getInt("id");
              String sql2 = "INSERT INTO products_selling_rate (product_id,selling_rate) values(?,?)";
              String sql3 = "INSERT INTO product_supplier(product_id, supplier_id) values(?,?)";
//              String sql4 = "INSERT INTO purch"
              PreparedStatement ps2 = conn.prepareStatement(sql2);
              ps2.setInt(1,prod_id);
              ps2.setInt(2,Integer.parseInt(sellingrate.getText()));
              ps2.execute();
              
              selectedSupplier = suppliers.getSelectionModel().getSelectedItem();
              String[] selectedSuppliers= selectedSupplier.split("\\s");
              String s2 = selectedSuppliers[0];
             
             PreparedStatement ps3 = conn.prepareStatement(sql3);
              ps3.setInt(1,prod_id);
              ps3.setInt(2,Integer.parseInt(s2));
              ps3.execute();
              
              
            }catch (SQLException ex) {
              Logger.getLogger(AddProductController.class.getName()).log(Level.SEVERE, null, ex);
          }
           
              
              
         
//          try {
//           FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("dashboard.fxml"));
//           Parent  root1 = (Parent) fxmlLoader.load();
//           Stage stage = new Stage();
//           stage.setScene(new Scene(root1));  
//           stage.show();
//          } catch (IOException ex) {
//              Logger.getLogger(AddEmployeeController.class.getName()).log(Level.SEVERE, null, ex);
//          }
      });

    }
}

    



