 //#EF3333

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

public class ManageCustomerController implements Initializable {
    
@FXML
private TableView<Customer> table;
@FXML
private TableColumn<Customer,Integer> id;

@FXML
private TableColumn<Customer,String> name;
@FXML
private TableColumn<Customer,String> email;
@FXML
private TableColumn<Customer,String> phoneNo;
@FXML
private TableColumn<Customer,String> gender;
@FXML
private TableColumn<Customer,String> gstNo;
@FXML
private TableColumn<Customer,String> address;
@FXML
private TableColumn<Customer,String> action;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private TextField hiddenTextfield;
    
    @FXML
private TextField searchfield;
    
    @FXML
    private TextField fname;

    @FXML
    private TextField fname1;

    @FXML
    private TextField phonenumber;

    @FXML
    private TextField emailid;

    @FXML
    private ComboBox<String> gender1;

    @FXML
    private TextField blockno;

    @FXML
    private TextField streetno;

    @FXML
    private TextField city;

    @FXML
    private TextField pincode;

    @FXML
    private TextField state;

    @FXML
    private TextField country;
    
    @FXML
    private TextField town;
    
    @FXML
    private TextField gst_no;
   
    
    

@Override
    public void initialize(URL url, ResourceBundle rb) {
        scrollPane.setVisible(false);
        gender1.getItems().removeAll(gender1.getItems());
        gender1.getItems().addAll("Male", "Female");
        showCustomers();
        hiddenTextfield.setVisible(false);  
    }
    
    public ObservableList<Customer> getCustomersList(){
        ObservableList<Customer> customers = FXCollections.observableArrayList();
         String sql = "select c.id, c.gst_no, concat(c.first_name, \" \", c.last_name) as full_name,c.phone_no,c.email_id,c.gender,concat(ad.block_no,\" ; \",ad.street,\" ; \",ad.city,\" ; \",ad.pincode,\" ; \",ad.state,\" ; \",ad.country,\" ; \",ad.town,\" ; \") as full_address from customers as c INNER JOIN address_customer as ac INNER JOIN address as ad ON c.id = ac.customer_id AND ad.id = ac.address_id AND c.deleted=0 AND ad.deleted = 0";
         ResultSet rs = DbConnect.execute(sql);
            try {
                while(rs.next()){
                
                    customers.add(new Customer(rs.getInt("id"),rs.getString("full_name"),rs.getString("gst_no"),rs.getString("phone_no"),rs.getString("email_id"),rs.getString("gender"),rs.getString("full_address")));
                    System.out.println(rs.getString("gst_no"));
                } 
                
            } catch (SQLException ex) {   
                Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
    }   
             
    return customers;    
 }
      public void dispose() {
        scrollPane.setVisible(false);
             
             PreparedStatement ps =  null;
             PreparedStatement ps2 =  null;
         try {
             if((!"".equals(fname.getText())) 
                     && (!"".equals(fname1.getText())) 
                     && (!"".equals(gst_no.getText())) 
                     && (!"".equals(emailid.getText())) 
                     && (!"".equals(phonenumber.getText()))
                     && (!"".equals(gender1.getValue()))
                     && (!"".equals(blockno.getText()))
                     && (!"".equals(streetno.getText()))
                     && (!"".equals(city.getText()))
                     && (!"".equals(state.getText()))
                     && (!"".equals(country.getText()))
                     && (!"".equals(town.getText()))
                     )  {
                 
            Connection conn = DbConnect.getConnection();
            java.util.Date dt = new java.util.Date();
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTime = sdf.format(dt);
            
             
            String sql = "UPDATE customers SET first_name = ?, last_name = ?, gst_no=?,email_id = ?, phone_no = ?, gender = ?, updated_at = ? where id = ? ";
            String sql1 = "SELECT address_id from address_customer where customer_id ="+ hiddenTextfield.getText();
            ResultSet rs = DbConnect.execute(sql1);
            rs.next();
            int addressId = rs.getInt("address_id");
            
            String sql2 = "UPDATE address SET block_no = ?, street = ?,city = ?, pincode=?, state=?, country=?, town=?, updated_at = ? where id= ?";
            
            
            ps = conn.prepareStatement(sql);
            ps2 = conn.prepareStatement(sql2);
            
            ps.setString(1,fname.getText());
            ps.setString(2,fname1.getText());
            ps.setString(3,gst_no.getText());
            ps.setString(4,emailid.getText());
            ps.setString(5,phonenumber.getText());
            ps.setString(6, (String) gender1.getValue());
            ps.setString(7, currentTime);
            ps.setString(8, hiddenTextfield.getText());
            
            ps2.setString(1,blockno.getText());
            ps2.setString(2,streetno.getText());
            ps2.setString(3,city.getText());
            ps2.setString(4,pincode.getText());
            ps2.setString(5,state.getText());
            ps2.setString(6,country.getText());
            ps2.setString(7,town.getText());
            ps2.setString(8,currentTime);
            ps2.setInt(9,addressId);
            
            ps.executeUpdate();
            ps2.executeUpdate();
            showCustomers();
          }
        } catch (SQLException ex) {
            Logger.getLogger(DbConnect.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
   
     
 public void showCustomers(){
    
    ObservableList<Customer> list = getCustomersList();
    
    id.setCellValueFactory(new PropertyValueFactory<>("id"));
    name.setCellValueFactory(new PropertyValueFactory<>("name"));
    gstNo.setCellValueFactory(new PropertyValueFactory<>("gstNo"));
    phoneNo.setCellValueFactory(new PropertyValueFactory<>("phoneNo"));
    email.setCellValueFactory(new PropertyValueFactory<>("email"));
    gender.setCellValueFactory(new PropertyValueFactory<>("gender"));
    address.setCellValueFactory(new PropertyValueFactory<>("address"));

     System.out.println(list);
//Addong buttons in each cell
Callback<TableColumn<Customer,String>,TableCell<Customer,String>> cellFactory = (param)->{
    
    final TableCell<Customer,String> cell = new TableCell<Customer,String>(){
        
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if(empty) {
                setGraphic(null);
                setText(null);
            }
            else {
                   Image img = new Image("trash.png");
                ImageView view = new ImageView(img);
                Image img1 = new Image("pen.png");
                ImageView view1 = new ImageView(img1);
                final Button deleteButton = new Button();
                final Button editButton = new Button();
                final HBox pane = new HBox(deleteButton, editButton);
                deleteButton.setStyle("-fx-background-color: transparent;");
                editButton.setStyle("-fx-background-color: transparent;");
                editButton.setGraphic(view1);
                deleteButton.setGraphic(view);
                
               
                deleteButton.setOnAction(event -> { 
                Customer c = getTableView().getItems().get(getIndex());
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Are you sure you want to delete " + " " + c.getName() + " " + " ? ");
                 String sql= "UPDATE customers SET deleted = 1 WHERE id = "+ c.getId();
                 Optional<ButtonType> result = alert.showAndWait();
                  if(!result.isPresent()) {
                     // alert is exited, no button has been pressed.
                  }
    
                    else if(result.get() == ButtonType.OK)
                    {
                        //oke button is pressed
                        DbConnect.delete(sql);
                        showCustomers();
                    }
     
                    else if(result.get() == ButtonType.CANCEL) {
                        // cancel button is pressed
                    }
                });
                 editButton.setOnAction(event -> { 
                 
                        Customer c = getTableView().getItems().get(getIndex());
                        scrollPane.setVisible(true);
                        
                        String customerName = c.getName();
                        String[] words = customerName.split("\\s");
                        fname.setText(words[0]);
                        fname1.setText(words[1]);
                        emailid.setText(c.getEmail());
                        
                        gender1.setValue(c.getGender());
                        gst_no.setText(c.getGstNo());
                        hiddenTextfield.setText(c.getId()+"");
                        phonenumber.setText(c.getPhoneNo()); 
                        String address = c.getAddress();
                        String addresswords[] = address.split(";");
                        
                        blockno.setText(addresswords[0]);
                        streetno.setText(addresswords[1]);
                        city.setText(addresswords[2]);
                        pincode.setText(addresswords[3]);
                        state.setText(addresswords[4]);
                        country.setText(addresswords[5]);
                        town.setText(addresswords[6]);  
    });
               
                setGraphic(pane);
                setText(null);
            }
        }
    };
 return cell;
};

FilteredList<Customer> filteredData = new FilteredList<>(list, e -> true);
            searchfield.setOnKeyReleased(e -> {
                searchfield.textProperty().addListener((observableValue, oldValue, newValue) -> {
                    filteredData.setPredicate((Predicate<? super Customer>) cus -> {
                        if(newValue == null || newValue.isEmpty()) {
                            return true;
                        }
                        String lowerCaseFilter = newValue.toLowerCase();
                        if(cus.getName().toLowerCase().contains(lowerCaseFilter)) {
                            return true;
                        }else if(cus.getEmail().contains(newValue)) {
                            return true;
                        }else if(cus.getAddress().toLowerCase().contains(lowerCaseFilter)) {
                            return true;
                        }else if(cus.getGstNo().contains(newValue)) {
                            return true;
                        }else if(cus.getGender().toLowerCase().contains(lowerCaseFilter)) {
                            return true;
                        }else if(cus.getPhoneNo().contains(newValue)) {
                            return true;
                        }
                        
                        return false;
                    });
                        
                });
                SortedList<Customer> sortedData = new SortedList<>(filteredData);
                sortedData.comparatorProperty().bind(table.comparatorProperty());
                table.setItems(sortedData);
                
                
                
            });
action.setCellFactory(cellFactory);
table.setItems(list);
    }    
}
