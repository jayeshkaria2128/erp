

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Callback;


public class ManageCategoryController implements Initializable {

@FXML
private TableView<Category> table;
@FXML
private TableColumn<Category,Integer> id;

@FXML
private TableColumn<Category,String> name;
    
@FXML
private TableColumn<Category,String> action;   
    
@FXML
private Pane form;

@FXML
private Pane horizontalPane;
    
@FXML
private Button submit;
 
@FXML
private TextField hiddenTextfield;

@FXML
private TextField searchfield;
 
@FXML
private TextField getName;
 
    
@Override
public void initialize(URL url, ResourceBundle rb) {
    showCategorys();
    form.setVisible(false);
        hiddenTextfield.setVisible(false);
        
    }   



    

    
    public ObservableList<Category> getCategoryList(){
        ObservableList<Category> categorys = FXCollections.observableArrayList();
         String sql = "SELECT id,name from category where deleted = 0"; 
         ResultSet rs = DbConnect.execute(sql);
            try {
                while(rs.next()){
                categorys.add(new Category(rs.getString("id"),rs.getString("name")));
                } 
            } catch (SQLException ex) {   
                Logger.getLogger(ManageCategoryController.class.getName()).log(Level.SEVERE, null, ex);
    }   
    return categorys;  
    
 }
    
   
    
    public void dispose(){
        form.setVisible(false);
//        horizontalPane.setVisible(true);

     
        PreparedStatement ps =  null;
         try {
             if((!"".equals(getName.getText()))) {
                 
            Connection conn = DbConnect.getConnection();
            String updatedCategory = "Muskaan";
            String updatedId = hiddenTextfield.getText();
             
             java.util.Date dt = new java.util.Date();
             java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
             String currentTime = sdf.format(dt);
             
            String sql = "UPDATE category set name= ?, updated_at = ? where id = ? ";
            ps = conn.prepareStatement(sql);
            ps.setString(1,getName.getText());
            ps.setString(3,hiddenTextfield.getText());
            ps.setString(2, currentTime);
            
            ps.executeUpdate();
            showCategorys();
          }
        } catch (SQLException ex) {
            Logger.getLogger(DbConnect.class.getName()).log(Level.SEVERE, null, ex);
        }    
     }
        
    

   
     
 public void showCategorys(){
    
    ObservableList<Category> list = getCategoryList();
    
    id.setCellValueFactory(new PropertyValueFactory<>("id"));
    name.setCellValueFactory(new PropertyValueFactory<>("categoryName"));


//Addong buttons in each cell
Callback<TableColumn<Category,String>,TableCell<Category,String>> cellFactory = (TableColumn<Category, String> param)->{
    
    final TableCell<Category,String> cell = new TableCell<Category,String>(){
        
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if(empty) {
                setGraphic(null);
                setText(null);
            }
            else {
                Image img = new Image("trash.png");
                ImageView view = new ImageView(img);
                Image img1 = new Image("pen.png");
                ImageView view1 = new ImageView(img1);
                final Button deleteButton = new Button();
                final Button editButton = new Button();
                final HBox pane = new HBox(deleteButton, editButton);
                deleteButton.setStyle("-fx-background-color: transparent;");
                editButton.setStyle("-fx-background-color: transparent;");
                editButton.setGraphic(view1);
                
                deleteButton.setGraphic(view);
                deleteButton.setOnAction(event -> { 
                Category c = getTableView().getItems().get(getIndex());
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Are you sure you want to delete " + " " + c.getCategoryName() + " " + "record ? ");
                 String sql= "UPDATE category SET deleted = 1 WHERE id = "+ c.getId();
                 Optional<ButtonType> result = alert.showAndWait();
                  if(!result.isPresent()) {
                     // alert is exited, no button has been pressed.
                  }
    
                    else if(result.get() == ButtonType.OK)
                    {
                        //oke button is pressed
                        DbConnect.delete(sql);
                        showCategorys();
                    }
     
                    else if(result.get() == ButtonType.CANCEL) {
                        // cancel button is pressed
                    }
                });
                editButton.setOnAction(event -> { 
                 
                        Category c = getTableView().getItems().get(getIndex());
                        form.setVisible(true);
//                      horizontalPane.setVisible(false);
                        getName.setText(c.getCategoryName());
                        hiddenTextfield.setText(c.getId()+"");
                        
                        
                        
                        
                });
                
//                setGraphic(deleteButton);
            

    

                setGraphic(pane);
                setText(null);
            }
        }
    };
 return cell;
};
FilteredList<Category> filteredData = new FilteredList<>(list, e -> true);
            searchfield.setOnKeyReleased(e -> {
                searchfield.textProperty().addListener((observableValue, oldValue, newValue) -> {
                    filteredData.setPredicate((Predicate<? super Category>) cat -> {
                        if(newValue == null || newValue.isEmpty()) {
                            return true;
                        }
                        String lowerCaseFilter = newValue.toLowerCase();
                        if(cat.getCategoryName().toLowerCase().contains(lowerCaseFilter)) {
                            return true;
                        }else if(cat.getId().contains(newValue)) {
                            return true;
                        }
                        
                        return false;
                    });
                        
                });
                SortedList<Category> sortedData = new SortedList<>(filteredData);
                sortedData.comparatorProperty().bind(table.comparatorProperty());
                table.setItems(sortedData);
                
                
                
            });
action.setCellFactory(cellFactory);
table.setItems(list);
    }    
    
}
