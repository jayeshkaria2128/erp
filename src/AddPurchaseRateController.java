/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.collections.FXCollections;
import javafx.scene.control.cell.PropertyValueFactory;
public class AddPurchaseRateController implements Initializable {

    @FXML
    private ComboBox<String> category;

    @FXML
    private ComboBox<String> products;

    @FXML
    private ComboBox<String> suppliers;
    
    @FXML
    private AnchorPane anchorpane;
    
    
    private String selectedCategory;
    
    private String selectedProduct;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        category.setValue("Choose Category:");
        suppliers.setValue("Choose Suppliers:");
        products.setValue("Choose Products:");
        Helper helper = new Helper();
        HashMap categoryList = new HashMap();
        categoryList = DbConnect.getCategory();
        helper.setDataInComboboxAsHashMap(category, categoryList);
    }   
    
    
    
    
    public void stateChanged() {
        Helper helper = new Helper();
        selectedCategory = category.getSelectionModel().getSelectedItem();
        String[] selectedCat= selectedCategory.split("\\s");
        String s = selectedCat[0];
        HashMap prod = DbConnect.getProductsAccorgingToCategory(s);
        helper.setDataInComboboxAsHashMap(products, prod);
    }
     public void stateChangedAll() {
        
        Helper helper = new Helper();
        HashMap<String, String> supplierList = new HashMap<>();
        selectedCategory =  category.getSelectionModel().getSelectedItem();
        selectedProduct = products.getSelectionModel().getSelectedItem();
        
        String[] selectedCat= selectedCategory.split("\\s");
        String s = selectedCat[0];
        String[] selectedProd= selectedProduct.split("\\s");
        String s1 = selectedProd[0];
        supplierList = DbConnect.getSuppliersAccordingToCategory(s, s1);
        System.out.println(supplierList);
        helper.setDataInComboboxAsHashMap(suppliers, supplierList);
    }
    
}
