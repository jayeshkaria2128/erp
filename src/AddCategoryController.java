

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;


public class AddCategoryController implements Initializable {
@FXML
Button submit;

@FXML 
TextField getName;


public void display(){
      submit.setOnAction(e->{
          try {
              String categoryName = " ";
              categoryName = getName.getText();
              System.out.println(categoryName);
              Connection conn = DbConnect.getConnection();
              String sql = "INSERT into category (name)" + "values(?)";
              PreparedStatement ps = conn.prepareStatement(sql);
              ps.setString(1,categoryName);
              ps.execute();
          } catch (SQLException ex) {
              Logger.getLogger(AddCategoryController.class.getName()).log(Level.SEVERE, null, ex);
          }
            
        });
}

        @Override
    public void initialize(URL url, ResourceBundle rb) {
      
    }    
    
}
