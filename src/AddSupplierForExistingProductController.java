//import java.net.URL;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.HashMap;
//import java.util.ResourceBundle;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javafx.fxml.FXML;
//import javafx.fxml.Initializable;
//import javafx.scene.control.Button;
//import javafx.scene.control.ComboBox;
//import javafx.scene.control.TextField;
//import org.controlsfx.validation.ValidationSupport;
//import org.controlsfx.validation.Validator;
//
//public class AddSupplierForExistingProductController implements Initializable {
//    
//     @FXML
//    private TextField specifications;
//
//    @FXML
//    private ComboBox<String> category;
//
//    @FXML
//    private ComboBox<String> suppliers;
//
//    @FXML
//    private Button submit;
//
//    @FXML
//    private TextField eoq;
//
//    @FXML
//    private TextField dangerlevel;
//
//    @FXML
//    private TextField quantity;
//
//    @FXML
//    private TextField sellingrate;
//
//    @FXML
//    private TextField hsncode;
//
//    @FXML
//    private ComboBox<String> fetchedproducts;
//    
//      @FXML
//    private ComboBox<String> associatedsuppliers;
//    
//    ValidationSupport validationsupport = new ValidationSupport();
//
//    private String selectedCategory;
//    
//    private String selectedProduct;
//    
//    private String selectedSupplier;
//    
//    @Override
//    public void initialize(URL url, ResourceBundle rb) {
//        
//        Helper helper = new Helper();
//        category.setValue("Choose Category:");
//        suppliers.setValue("Choose Suppliers:");
//        fetchedproducts.setValue("Choose Products :");
//        
//        HashMap<String, String> categoryList = new HashMap<>();
//        categoryList = DbConnect.getCategory();
//        helper.setDataInComboboxAsHashMap(category, categoryList);
//        
//        validationsupport.registerValidator(category, Validator.createEmptyValidator("Text is required"));
//        validationsupport.registerValidator(fetchedproducts, Validator.createEmptyValidator("Text is required"));
//    }    
//    
//    
//    
//     public void getSupplierList(){
//         Helper helper = new Helper();
//        selectedCategory = category.getSelectionModel().getSelectedItem();
//        String[] selectedCat= selectedCategory.split("\\s");
//        String s = selectedCat[0];
//        HashMap prod = DbConnect.getProductsAccorgingToCategory(s);
//        helper.setDataInComboboxAsHashMap(fetchedproducts, prod);
//      
//        
//    }
//     
//     public void getAll(){
////         String sql = "SELECT name , specification, hsn_code, category_id, eoq_level, danger_level, quantity FROM products\n" +
////        "WHERE category_id = ? and id = ?";
//        Helper helper = new Helper();
//        HashMap<String, String> supplierList = new HashMap<>();
//        selectedCategory = category.getSelectionModel().getSelectedItem();
//        String[] words=selectedCategory.split("\\s");
//        String Selectedcate = words[0];
//        selectedProduct = fetchedproducts.getSelectionModel().getSelectedItem();
//        String[] selectedProd= selectedProduct.split("\\s");
//        String s1 = selectedProd[0];
//        supplierList = DbConnect.getSuppliersNotAssociated(Selectedcate, s1);
//        helper.setDataInComboboxAsHashMap(suppliers, supplierList);
//        
//        supplierList = DbConnect.getSuppliersAssociated(Selectedcate, s1);
//        helper.setDataInComboboxAsHashMap(associatedsuppliers, supplierList);
//        
//         String sql = "SELECT name , specification, products_selling_rate.selling_rate ,  hsn_code, category_id, eoq_level, danger_level, quantity FROM products  \n" +
//         "INNER JOIN  products_selling_rate on products.id = products_selling_rate.product_id  \n" +
//         "AND category_id = ? and products.id = ?";
//         
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        
//        try{
//          Connection conn = DbConnect.getConnection();
//          ps = conn.prepareStatement(sql);
//          ps.setString(1,Selectedcate);
//          ps.setString(2,s1);
//          rs = ps.executeQuery();
//          while(rs.next()){
//            hsncode.setText(rs.getString("hsn_code"));
//            eoq.setText(rs.getString("eoq_level"));
//            dangerlevel.setText(rs.getString("danger_level"));
//            specifications.setText(rs.getString("specification"));
//            sellingrate.setText(rs.getString("selling_rate"));
//            quantity.setText(rs.getString("quantity"));
//          }
//          
//        }catch (SQLException ex) {
//             Logger.getLogger(AddSupplierForExistingProductController.class.getName()).log(Level.SEVERE, null, ex);
//         }
//    }
//     
//     public void insertInToProductSupplier() {
//         
//         String sql3 = "INSERT INTO product_supplier(product_id, supplier_id) values(?,?)";
//         selectedSupplier = suppliers.getSelectionModel().getSelectedItem();
//         String[] sup= selectedSupplier.split("\\s");
//         String selectedSup = sup[0];
//         
//         selectedProduct = fetchedproducts.getSelectionModel().getSelectedItem();
//        String[] selectedProd= selectedProduct.split("\\s");
//        String s1 = selectedProd[0];
//         
//         
//         
//         PreparedStatement ps1 = null;
//         ResultSet rs = null;
//        
//        try{
//          Connection conn = DbConnect.getConnection();
//          ps1 = conn.prepareStatement(sql3);
//          ps1.setString(2,selectedSup);
//          ps1.setString(1,s1);
//          ps1.execute();
//         
//     }catch (SQLException ex) {
//             Logger.getLogger(AddSupplierForExistingProductController.class.getName()).log(Level.SEVERE, null, ex);
//         }
//    }
//}
