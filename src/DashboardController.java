/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author preeti
 * 
 * 
 */
   

public class DashboardController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private Pane pane;
    
//    @FXML
//    private Label dashbaord;

    @FXML
    private Label addcategories;
     
    @FXML
    private Label manageCategories;
    
    @FXML
    private Label ManageSuppliers;

    @FXML
    private Label addSuppliers;

    @FXML
    private Label AddProducts;
    
    @FXML
    private Label ManageProducts;

    @FXML
    private Label AddSales;

    @FXML
    private Label AddPurchases;

    @FXML
    private Label AddCustomers;

    @FXML
    private Label ManageCustomers;
       
    @FXML
    private Label lblSql;
    
    @FXML
    private Label lblSql2;

    @FXML
    private Label lblSql3;

    @FXML
    private Label lblSql4;    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
      
        try {
            Connection conn = DbConnect.getConnection();
            PreparedStatement ps = null;
            String sql = "SELECT COUNT(id) as count FROM suppliers";
            String sql2 = "SELECT COUNT(id) as count2 FROM customers";
            String sql3 = "SELECT COUNT(id) as count3 FROM products";
            String sql4 = "SELECT COUNT(id) as count4 FROM employees";            
            
            ResultSet rs = DbConnect.execute(sql);
            rs.next();
       
            ResultSet rs2 = DbConnect.execute(sql2);
            rs2.next();
            
            ResultSet rs3 = DbConnect.execute(sql3);
            rs3.next();
            
            ResultSet rs4 = DbConnect.execute(sql4);
            rs4.next();
            
            lblSql.setText(rs.getString("count") + " Suppliers");
            lblSql2.setText(rs2.getString("count2") + " Customers");
            lblSql3.setText(rs3.getString("count3") + " Products");
            lblSql4.setText(rs4.getString("count4") + " Employees");
            
        } catch (SQLException ex) {
            Logger.getLogger(DashboardController.class.getName()).log(Level.SEVERE, null, ex);
        }
     }    
   
//    public void dashboardListener() throws IOException{
//        FXMLLoader loader = new FXMLLoader(getClass().getResource("Dashboard.fxml"));
//        
//        Node root = loader.load();
//        
////        Stage stage = (Stage) pane.getScene().getWindow();
////        Scene scene = new Scene(root);
////        stage.setScene(scene);
//        
//        Platform.runLater(()->{
//            pane.getChildren().clear();
//            pane.getChildren().add(root);        
//        });
//    }
    
    
    public void addCategoriesListener() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("AddCategory.fxml"));
        
        Node root = loader.load();
        
//        Stage stage = (Stage) pane.getScene().getWindow();
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
        
        Platform.runLater(()->{
            pane.getChildren().clear();
            pane.getChildren().add(root);        
        });
    }
    
    public void manageCategoriesListener() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ManageCategory.fxml"));
        
        Node root = loader.load();
        
//        Stage stage = (Stage) pane.getScene().getWindow();
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
        
        Platform.runLater(()->{
            pane.getChildren().clear();
            pane.getChildren().add(root);        
        });
    }
    
        public void addSuppliersListener() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("AddSupplier.fxml"));
        
        Node root = loader.load();
        
//        Stage stage = (Stage) pane.getScene().getWindow();
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
        
        Platform.runLater(()->{
            pane.getChildren().clear();
            pane.getChildren().add(root);        
        });
    }
    
    public void manageSuppliersListener() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ManageSupplier.fxml"));
        
        Node root = loader.load();
        
//        Stage stage = (Stage) pane.getScene().getWindow();
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
        
        Platform.runLater(()->{
            pane.getChildren().clear();
            pane.getChildren().add(root);        
        });
    }
    
     public void addProductsListener() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("AddProduct.fxml"));
        
        Node root = loader.load();
        
//        Stage stage = (Stage) pane.getScene().getWindow();
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
        
        Platform.runLater(()->{
            pane.getChildren().clear();
            pane.getChildren().add(root);        
        });
    }
    
    public void manageProductsListener() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ManageProduct.fxml"));
        
        Node root = loader.load();
        
//        Stage stage = (Stage) pane.getScene().getWindow();
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
        
        Platform.runLater(()->{
            pane.getChildren().clear();
            pane.getChildren().add(root);        
        });
    }
    
    public void addSalesListener() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("AddSales.fxml"));
        
        Node root = loader.load();
        
//        Stage stage = (Stage) pane.getScene().getWindow();
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
        
        Platform.runLater(()->{
            pane.getChildren().clear();
            pane.getChildren().add(root);        
        });
    }
    
    public void addPurchasesListener() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("AddPurchase.fxml"));
        
        Node root = loader.load();
        
//        Stage stage = (Stage) pane.getScene().getWindow();
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
        
        Platform.runLater(()->{
            pane.getChildren().clear();
            pane.getChildren().add(root);        
        });
    }
    public void addCustomersListener() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("AddCustomer.fxml"));
        
        Node root = loader.load();
        
//        Stage stage = (Stage) pane.getScene().getWindow();
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
        
        Platform.runLater(()->{
            pane.getChildren().clear();
            pane.getChildren().add(root);        
        });
    }
    
    public void manageCustomersListener() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ManageCustomer.fxml"));
        
        Node root = loader.load();
        
//        Stage stage = (Stage) pane.getScene().getWindow();
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
        
        Platform.runLater(()->{
            pane.getChildren().clear();
            pane.getChildren().add(root);        
        });
    }
}
