import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author Jayesh Karia
 */
public class ManageEmployeeController implements Initializable {

@FXML
private TableView<Employee> table;
@FXML
private TableColumn<Employee,Integer> id;

@FXML
private TextField searchfield;

@FXML
private TableColumn<Employee,String> name;
@FXML
private TableColumn<Employee,String> email;
@FXML
private TableColumn<Employee,String> phoneNo;
@FXML
private TableColumn<Employee,String> gender;
@FXML
private TableColumn<Employee,String> address;
@FXML
private TableColumn<Employee,String> action;
  @FXML
    private ScrollPane scrollPane;

    @FXML
    private TextField fname;

    @FXML
    private TextField fname1;

    @FXML
    private TextField phonenumber;

    @FXML
    private TextField emailid;

    @FXML
    private ComboBox<String> gender1;

    @FXML
    private TextField town;

    @FXML
    private TextField blockno;

    @FXML
    private TextField streetno;

    @FXML
    private TextField city;

    @FXML
    private TextField pincode;

    @FXML
    private TextField state;

    @FXML
    private TextField country;
    
    @FXML
    private TextField hiddenTextfield;
    
    





    @Override
    public void initialize(URL url, ResourceBundle rb) {
        scrollPane.setVisible(false);
        gender1.getItems().removeAll(gender1.getItems());
        gender1.getItems().addAll("Male", "Female");
        showEmployees();
        hiddenTextfield.setVisible(false);  
    }    
    
    
    
    public ObservableList<Employee> getEmployeesList(){
        ObservableList<Employee> employees = FXCollections.observableArrayList();
         String sql = "SELECT e.id,CONCAT(e.first_name, \" \", e.last_name) as full_name, e.email_id, e.phone_no, e.gender, concat(a.block_no,\" ; \",a.street,\"; \", a.city, \" ;\", a.pincode, \"; \", a.state, \" ;\", a.country, \" ;\", a.town ) as full_address from employees as e INNER JOIN address as a ON e.address_id = a.id AND e.deleted=0 AND a.deleted = 0";
         ResultSet rs = DbConnect.execute(sql);
            try {
                while(rs.next()){
                employees.add(new Employee(rs.getInt("id"),rs.getString("full_name"),rs.getString("phone_no"),rs.getString("email_id"),
                        rs.getString("gender"),rs.getString("full_address")));
                } 
            } catch (SQLException ex) {   
                Logger.getLogger(ManageEmployeeController.class.getName()).log(Level.SEVERE, null, ex);
    }   
    return employees;    
 }
     public void dispose() {
        scrollPane.setVisible(false);
             
             PreparedStatement ps =  null;
             PreparedStatement ps2 =  null;
         try {
             if((!"".equals(fname.getText())) 
                     && (!"".equals(fname1.getText())) 
                     && (!"".equals(emailid.getText())) 
                     && (!"".equals(phonenumber.getText()))
                     && (!"".equals(gender1.getValue()))
                     && (!"".equals(blockno.getText()))
                     && (!"".equals(streetno.getText()))
                     && (!"".equals(city.getText()))
                     && (!"".equals(state.getText()))
                     && (!"".equals(country.getText()))
                     && (!"".equals(town.getText()))
                     )  {
                 
            Connection conn = DbConnect.getConnection();
            java.util.Date dt = new java.util.Date();
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTime = sdf.format(dt);
            
             
            String sql = "UPDATE employees SET first_name = ?, last_name = ?, email_id = ?, phone_no = ?, gender = ?, updated_at = ? where id = ? ";
            String sql1 = "SELECT address_id from employees where id ="+ hiddenTextfield.getText();
            ResultSet rs = DbConnect.execute(sql1);
            rs.next();
            int addressId = rs.getInt("address_id");
            System.out.println(addressId);
            String sql2 = "UPDATE address SET block_no = ?, street = ?,city = ?, pincode=?, state=?, country=?, town=?, updated_at = ? where id= ?";
            
            
            ps = conn.prepareStatement(sql);
            ps2 = conn.prepareStatement(sql2);
            
            ps.setString(1,fname.getText());
            ps.setString(2,fname1.getText());
            ps.setString(3,emailid.getText());
            ps.setString(4,phonenumber.getText());
            ps.setString(5,gender1.getValue());
            ps.setString(6, currentTime);
            ps.setString(7, hiddenTextfield.getText());
            
            ps2.setString(1,blockno.getText());
            ps2.setString(2,streetno.getText());
            ps2.setString(3,city.getText());
            ps2.setString(4,pincode.getText());
            ps2.setString(5,state.getText());
            ps2.setString(6,country.getText());
            ps2.setString(7,town.getText());
            ps2.setString(8,currentTime);
            ps2.setInt(9,addressId);
            
            
            ps.executeUpdate();
            ps2.executeUpdate();
            showEmployees();
          }
        } catch (SQLException ex) {
            Logger.getLogger(DbConnect.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
   
     
 public void showEmployees(){
    
    ObservableList<Employee> list = getEmployeesList();
    
    id.setCellValueFactory(new PropertyValueFactory<>("id"));
    name.setCellValueFactory(new PropertyValueFactory<>("name"));
    phoneNo.setCellValueFactory(new PropertyValueFactory<>("phoneNo"));
    email.setCellValueFactory(new PropertyValueFactory<>("email"));
    gender.setCellValueFactory(new PropertyValueFactory<>("gender"));
    address.setCellValueFactory(new PropertyValueFactory<>("address"));


//Addong buttons in each cell
Callback<TableColumn<Employee,String>,TableCell<Employee,String>> cellFactory = (param)->{
    
    final TableCell<Employee,String> cell = new TableCell<Employee,String>(){
        
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if(empty) {
                setGraphic(null);
                setText(null);
            }
            else {
                Image img = new Image("trash.png");
                ImageView view = new ImageView(img);
                Image img1 = new Image("pen.png");
                ImageView view1 = new ImageView(img1);
                final Button deleteButton = new Button();
                final Button editButton = new Button();
                final HBox pane = new HBox(deleteButton, editButton);
                deleteButton.setStyle("-fx-background-color: transparent;");
                editButton.setStyle("-fx-background-color: transparent;");
                editButton.setGraphic(view1);
                
                deleteButton.setGraphic(view);

                deleteButton.setOnAction(event -> { 
                Employee e = getTableView().getItems().get(getIndex());
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Are you sure you want to delete " + " " + e.getName() + " " + " ? ");
                 String sql= "UPDATE employees SET deleted = 1 WHERE id = "+ e.getId();
                 Optional<ButtonType> result = alert.showAndWait();
                  if(!result.isPresent()) {
                     // alert is exited, no button has been pressed.
                  }
    
                    else if(result.get() == ButtonType.OK)
                    {
                        //oke button is pressed
                        DbConnect.delete(sql);
                        showEmployees();
                    }
     
                    else if(result.get() == ButtonType.CANCEL) {
                        // cancel button is pressed
                    }
                });
                 editButton.setOnAction(event -> { 
                 
                        Employee e = getTableView().getItems().get(getIndex());
                        scrollPane.setVisible(true);
                        String employeeName = e.getName();
                        String[] words = employeeName.split("\\s");
                            fname.setText(words[0]);
                            fname1.setText(words[1]);
                        emailid.setText(e.getEmail());
                        gender1.setValue(e.getGender());
                        hiddenTextfield.setText(e.getId()+"");
                        phonenumber.setText(e.getPhoneNo()); 
                        String address = e.getAddress();
                        String addresswords[] = address.split(";");
                        blockno.setText(addresswords[0]);
                        streetno.setText(addresswords[1]);
                        city.setText(addresswords[2]);
                        pincode.setText(addresswords[3]);
                        state.setText(addresswords[4]);
                        country.setText(addresswords[5]);
                        town.setText(addresswords[6]);  
    });
               
       setGraphic(pane);
       setText(null);
            }
        }
    };
 return cell;
};

FilteredList<Employee> filteredData = new FilteredList<>(list, e -> true);
            searchfield.setOnKeyReleased(e -> {
                searchfield.textProperty().addListener((observableValue, oldValue, newValue) -> {
                    filteredData.setPredicate((Predicate<? super Employee>) emp -> {
                        if(newValue == null || newValue.isEmpty()) {
                            return true;
                        }
                        String lowerCaseFilter = newValue.toLowerCase();
                        if(emp.getName().toLowerCase().contains(lowerCaseFilter)) {
                            return true;
                        }else if(emp.getEmail().contains(newValue)) {
                            return true;
                        }else if(emp.getAddress().toLowerCase().contains(lowerCaseFilter)) {
                            return true;
                        }else if(emp.getPhoneNo().contains(newValue)) {
                            return true;
                        }else if(emp.getGender().contains(newValue)) {
                            return true;
                        } 
//                        else if(emp.getId().contains(newValue)) {
//                            return true;
//                        }
//                        
                        return false;
                    });
                        
                });
                SortedList<Employee> sortedData = new SortedList<>(filteredData);
                sortedData.comparatorProperty().bind(table.comparatorProperty());
                table.setItems(sortedData);
                
                
                
            });

action.setCellFactory(cellFactory);
table.setItems(list);
    }    
}

    

